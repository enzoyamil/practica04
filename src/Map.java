import java.util.*;

public class Map {

    public void printMap(HashMap<String,Country> map){
        //System.out.println(map.values());
        for (HashMap.Entry<String,Country> index : map.entrySet()) {
            System.out.println("Key: "+ index.getKey()+" "+ "Value: "+index.getValue().getNameCountry()+"Continente: "+index.getValue().getContinent());
        }
    }
    public void printMapOrganized(HashMap<String,Country> map){
        System.out.println("Mapa Ordenado");
        //System.out.println(map.values());
        HashMap<String,Country> mapOrder= ordenarMapaPorContinentes(map);
        for (HashMap.Entry<String,Country> index : mapOrder.entrySet()) {
            System.out.println("Key: "+ index.getKey()+" "+ "Value: "+index.getValue().getNameCountry()+"Continente: "+index.getValue().getContinent());
        }
    }
    private HashMap<String,Country> ordenarMapaPorContinentes(HashMap<String,Country> map){
        List<java.util.Map.Entry<String,Country>> mapResult =
                new LinkedList<java.util.Map.Entry<String, Country>>(map.entrySet());
        Collections.sort(mapResult, new Comparator<java.util.Map.Entry<String, Country>>() {
            @Override
            public int compare(java.util.Map.Entry<String, Country> o1, java.util.Map.Entry<String, Country> o2) {
                return o1.getValue().getContinent().compareTo(o2.getValue().getContinent());
            }
        });
        HashMap<String,Country> ordermap = new LinkedHashMap<>();
        for(HashMap.Entry<String,Country> item :mapResult){
            ordermap.put(item.getKey(),item.getValue());
        }

        return ordermap;
    }
    public  HashMap<String,Country> joinMaps(HashMap<String,Country> map1,HashMap<String,Country> map2){
        //HashMap<String,Country> mapResult;
        for(HashMap.Entry<String,Country> element: map2.entrySet()){
            map1.put(element.getKey(),element.getValue());
        }
         return map1;
    }

}
