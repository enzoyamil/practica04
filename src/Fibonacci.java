import java.util.ArrayList;
import java.util.LinkedList;

public class Fibonacci {
    private ArrayList<Integer> list;
    private LinkedList<Integer>listConvert;
    private int size;
    public  Fibonacci(int size){
        this.size= size;
        list = new ArrayList<Integer>();
        listConvert=new LinkedList<Integer>();
    }

    public void showList(){
        fillInList();
        System.out.println("ArrarList");
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i)+" ");
        }
        System.out.println();
    }
    public void showListConvert(){
        convertList();
        System.out.println("LikendList");
        for (int i = 0; i <listConvert.size(); i++) {
            System.out.print(listConvert.get(i)+" ");
        }
        System.out.println();
        System.out.println("Primer Elemento:"+listConvert.getFirst());
        System.out.println("Ultimo Elemento:"+listConvert.getLast());
    }

    private void fillInList(){
        for (int i=0; i<size;i++){
            list.add(fibonacci(i));
        }
    }
    private  void convertList(){
        for ( int i=0;i<list.size();i++) {
            listConvert.add(list.get(i));

        }
    }
    private int fibonacci(int n)
    {
        if (n>1){
            return fibonacci(n-1) + fibonacci(n-2);
        }
        else if (n==1) {
            return 1;
        }
        else if (n==0){
            return 0;
        }
        else{ //error
             return -1;
        }
    }
}
