public class Country {

    private String nameCountry;
    private String continent;
    private int population;

    public Country(String nameCountry, String continent,int population){
        this.nameCountry=nameCountry;
        this.continent=continent;
        this.population=population;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }
}
